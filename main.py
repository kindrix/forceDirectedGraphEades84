#!/usr/bin/python
import random
from drawableNode import DrawableNode
from vector import Vector
from Tkinter import *
import time
import fdg
import gui
import helper

#window and node
WIDTH = 600
HEIGHT = 600
RAD = 15

# a = DrawableNode("1", RAD , Vector(100,100))
# b = DrawableNode("2", RAD ,  Vector(600,600))
# c = DrawableNode("3", RAD ,  Vector(400,400))
#
# nodeDict ={"1":a, "2":b, "3":c}
# graph={"1":[a], "2":[b,c], "3":[a]}

#read data
(nodeData, randomCoords)= helper.readData()

#create graph
(graph, nodeDict) = helper.createGraph(nodeData, randomCoords, RAD, WIDTH, HEIGHT)



#set up drawing window and canvas
tk = Tk()
gui = gui.GUI(tk,graph, nodeDict)


#apply force directed graphing algorithm
#gui.applyFDG()

tk.mainloop()
