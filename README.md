# Force Directed Graphing implementation
*An implementation of a force directed algorithm based on Eades (1984)*

**Note**: Not perfect - still working on it.

## Usage

Execute *main.py* along with one of the files in the *data/* folder. It will randomly assign coordinates to the nodes and display them.

To run the forced directed algorithm, *press 's' or 'S'* to run it for a short time. To double the run time of the algorithm, *press 'l' or 'L'*. The run time is proportional to the number of vertices of the graph.

## References
1. S. G. Kobourov, *Force-Directed Drawing Algorithms*, In Roberto Tamassia (editor), Handbook of Graph Drawing and Visualization, p. 383-408, CRC Press, 2013
2. P. Eades, *A Heuristic for Graph Drawing*, 1984

There is a demo file, *main.py*, to see the implementation in action. You can drag around the  nodes by holding down the mouse and moving it around. The edges will readjust automatically.

![alt text](https://gitlab.com/kindrix/forceDirectedGraphEades84/raw/cd7c781068bb276eb2b1b1f27ef180a8b25f3af7/fdg.gif "FDG Animation")



## Files
1. vector.py

    Contains Vector class consisting of two points and basic vector operations.
2. node.py

    Contains Node class consisting of a location, velocity and acceleration vectors.
3. drawableNode.py

    Contains DrawableNode class, a subclass of the Node class but also contains attributes to help with drawing it on the screen.

4. fdg.py

    Implementation of the force directed algorithm. The basic idea is that adjacent nodes attact each other whereas non-adjacent nodes repel each other. The direction of movement of the node is determined by the netforce on the node.

5. main.py

    Demo file. Execute using *./main.py filename* to see display graph. You can find some files in the data folder.

6. helper.py

    Contains some functions to help with reading data from file and creating the graph and drawing initial nodes.

6. data/

    Contains some test data.

    The first line of the file specifies whether the nodes have hard coded coordinates or not.

    *Random=True*

    If not, random coordinates will be used for the nodes.

    The rest of the lines contain node data in the following format:

    *nodeName xCoordinate yCoordinate: neighbourNode1, neighbourNode2, ...*

    Eg: a -1 -1: b c d

    (-1, -1) are just used as filler coordinates when *Random = False*
