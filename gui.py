import random
from drawableNode import DrawableNode
from vector import Vector
from Tkinter import *
import time
import fdg

class GUI():
    def __init__(self, master, graph, nodeDict, WIDTH=600, HEIGHT=600):
        self.master = master;
        self.graph = graph
        self.nodeDict = nodeDict
        self.WIDTH = WIDTH
        self.HEIGHT = HEIGHT
        master.title("Drawing")
        master.bind_all('<KeyPress>', self.keyPressedFDG)

        self.canvas = Canvas(master, width  = WIDTH, height = HEIGHT)
        self.canvas.pack()

        #draw initial nodes
        for u in nodeDict.values():
            DrawableNode.drawNode(u, self.canvas)

        #draw edges
        for nodeName in nodeDict:
            for node in graph[nodeName]:
                DrawableNode.drawEdge(nodeDict[nodeName], node, self.canvas)

        #bind node drag events
        for u in nodeDict.values():
            self.canvas.tag_bind(u.oval, '<B1-Motion>', lambda event, obj=u, c = self.canvas: self.dragNode(event, obj, c))

    #event method for moving node
    def dragNode(self,event, u, canvas):
        deltaX = event.x - u.loc.x
        deltaY = event.y - u.loc.y
        DrawableNode.moveNode(u, self.graph[u.name], canvas, deltaX, deltaY)


    #event method to applyFDG if key is pressed
    def keyPressedFDG(self, event):
        key = str(event.char)
        #s is for short
        if (key == 's' or key =='S'):
            self.applyFDG(5)
        #l is for long
        elif (key == 'l' or key =='L'):
            self.applyFDG(15)


    #turn on fdg
    def applyFDG(self, runFactor):
        #------- eades -----------------
        fdgOn = True;
        if fdgOn == True:
            #copy old locs
            tempNodeList={}
            for u in self.nodeDict.values():
                tempNodeList[u.name] = DrawableNode.getSimpleCopy(u)
            # for u in nodeDict.values():
            #     print "old:",u.loc, ", new:", tempNodeList[u.name].loc
            for i in range(len(self.nodeDict)*runFactor):
                #time.sleep(.001)
                #print "------loop----"
                netForce=fdg.getNetForce(self.nodeDict, self.graph)
                print "netforce:", netForce
                for nodeName in tempNodeList:
                    u = tempNodeList[nodeName]
                    u.applyForce(netForce[u.name])
                    u.updateLocation()
                    # print "old:", netForce
                DrawableNode.scaleToCanvas(tempNodeList.values(), self.WIDTH, self.HEIGHT)
                for u in self.nodeDict.values():
                    v = tempNodeList[u.name]
                    #print u.name, " old: ",u.loc, ", new:",  v.loc
                    deltaX = v.loc.x - u.loc.x
                    deltaY = v.loc.y - u.loc.y
                    #print u.name, " deltaX: ",deltaX, ", dletaY:",  deltaY
                    DrawableNode.moveNode(u, self.graph[u.name], self.canvas, deltaX, deltaY )
                self.master.update()
        #------- eades -----------------
